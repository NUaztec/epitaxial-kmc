/*
 *
 * Copyright © 2017-2018 by Northwestern University. All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    The following paper should be cited:

 	Soyoung E. Seo, Martin Girard, Monica Olvera de la Cruz, Chad A. Mirkin, "Non-equilibrium anisotropic colloidal single
 	crystal growth with DNA", Nature Communications, in press

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include "KMCIntegrator.hpp"
#include <x86intrin.h>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <thread>
#include <algorithm>
#include <iostream>
#include <ctime>

// couple of defines for clion to resolve includes
#ifdef __CLION_IDE__
#define __AVX2__
#define __AVX__
#define __SSE__
#define __SSE2__
#define __SSE3__
#define __SSE4A__
#define __SSE4_1__
#define __SSE4_2__
#define __SSSE3__
#define __x86_64__
#endif

// macro define AVX movement size, set this to 16 if instructions are replaced by AVX512
// asm section should be rewritten if AVX512 is used instead, random number generation should
// also be changed as a single instruction would generate twice as many random numbers
#define INSTR_SET_SIZE 8

// MC moves need 4 random numbers, which we want to generate all at once, functions by Agner Fog
#include "vectorclass.h"
#include "ranvec1.h"
#include "vectormath_exp.h"

// random is normally double precision, but allow for single
#ifdef SINGLE_PRECISION_RANDOM
#define RTYPE float
#else
#define RTYPE double
#endif

bool KMC_Integrator::do_license_display = true;

void KMC_Integrator::set_surface_dimensions(std::size_t Nx, std::size_t Ny)
{
	if(!grid)
		_mm_free(grid - INSTR_SET_SIZE);

	_mm_free(grid_prob_mov_up);
	_mm_free(grid_prob_mov_down);
	_mm_free(grid_up_cumsum);
	_mm_free(grid_down_cumsum);

	grid_dim_x = Nx;
	grid_dim_y = Ny;
	grid = (typeof(grid)) _mm_malloc((Nx * Ny + INSTR_SET_SIZE * 2) * sizeof(*grid), 64);
	// memset the grid to zero.
	memset(grid, 0, sizeof(*grid)*(Nx*Ny + INSTR_SET_SIZE *2));
	grid += INSTR_SET_SIZE;

	grid_prob_mov_up = (typeof(grid_prob_mov_up))  _mm_malloc(Nx*Ny * sizeof(*grid_prob_mov_up), 64);
	grid_prob_mov_down = (typeof(grid_prob_mov_down))  _mm_malloc(Nx*Ny * sizeof(*grid_prob_mov_down), 64);

	grid_up_cumsum = (typeof(grid_up_cumsum)) _mm_malloc(Nx*Ny * sizeof(*grid_up_cumsum), 64);
	grid_down_cumsum = (typeof(grid_down_cumsum)) _mm_malloc(Nx*Ny * sizeof(*grid_down_cumsum), 64);
}

void KMC_Integrator::set_surface_dimensions(std::size_t L)
{
	set_surface_dimensions(L, L);
}

std::size_t KMC_Integrator::get_numel()
{
	return grid_dim_x * grid_dim_y;
}

void KMC_Integrator::set_surface_and_bulk_energies(float *senergies, float* benergies)
{
	free(surface_energies);
	surface_energies = (typeof(surface_energies)) malloc(NLayer_types * sizeof(*surface_energies));
	bulk_energy = (typeof(bulk_energy)) malloc(NLayer_types * sizeof(*bulk_energy));

	memcpy(surface_energies, senergies, NLayer_types * sizeof(*surface_energies));
	memcpy(bulk_energy, benergies, NLayer_types * sizeof(*bulk_energy));
}

void KMC_Integrator::set_surface_edge_energies(float _edge_energies_x, float _edge_energies_y = 0.0f)
{

	edge_energy_x = _edge_energies_x;
	if(_edge_energies_y == 0.0f)
		edge_energy_y = _edge_energies_x;
	else
		edge_energy_y = _edge_energies_y;
	return;
}


// enable calculation of number of edges of each point
void KMC_Integrator::requireEdgeGrid(bool r)
{
	if(needsEdgeGrid && r) // calculation of edge grids is already set
		return;
	if(!edge_grid)
		_mm_free(edge_grid);
	needsEdgeGrid = r;
	if(r)
		edge_grid = (typeof(edge_grid)) _mm_malloc(sizeof(*edge_grid) * grid_dim_x * grid_dim_y, 32);

}

static void integrate_helper(std::size_t thread_idx, KMC_Integrator* object_ptr)
{

	std::size_t offset = object_ptr->grid_dim_x * object_ptr->grid_dim_y / object_ptr->nthreads;
	std::size_t nel = object_ptr->grid_dim_x * object_ptr->grid_dim_y;
	std::size_t nx = object_ptr->grid_dim_x;

	__m256i const_p1 = _mm256_set1_epi32(1); // vector of int32 ones
	__m256 energy_edge_x = _mm256_set1_ps(object_ptr->edge_energy_x); // vector of edge energies
	__m256 energy_edge_y = _mm256_set1_ps(object_ptr->edge_energy_y);


	std::int32_t* grid_init_ptr = object_ptr->grid + offset * thread_idx;
	float* raise_ptr = object_ptr->grid_prob_mov_up + offset * thread_idx;
	float* lower_ptr = object_ptr->grid_prob_mov_down + offset * thread_idx;

	typeof(KMC_Integrator::edge_grid) edge_grid = object_ptr->edge_grid + offset * thread_idx;

	__m256 cumulative_sum_down = _mm256_set1_ps(0.0f);
	__m256 cumulative_sum_up = _mm256_set1_ps(0.0f);

	float* up_cumsum_ptr = object_ptr->grid_up_cumsum + offset * thread_idx;
	float* down_cumsum_ptr = object_ptr->grid_down_cumsum + offset * thread_idx;



	// calculate changes in edge energy by raising or lowering the plane
	// instructions are all AVX/AVX2 assembly, expect illegal instruction errors on older CPU
	for(auto sse_index = 0; sse_index != offset / 8; sse_index++)
	{

		// load square
		__m256i cval = _mm256_load_si256((__m256i*) grid_init_ptr);

		std::size_t ptrp1 = ((std::size_t) (grid_init_ptr + 1 - object_ptr->grid)) % nel;
		__m256i cvalp1 = _mm256_loadu_si256(reinterpret_cast<__m256i*>(object_ptr->grid + ptrp1));

		std::size_t ptrm1 = ((std::size_t) (grid_init_ptr + nel - 1 - object_ptr->grid )) % nel;
		__m256i cvalm1 = _mm256_loadu_si256(reinterpret_cast<__m256i*>(object_ptr->grid + ptrm1));

		std::size_t ptrpL = ((std::size_t) (grid_init_ptr + nx - object_ptr->grid)) % nel;
		__m256i cvalpL = _mm256_loadu_si256(reinterpret_cast<__m256i*>(object_ptr->grid + ptrpL));

		std::size_t ptrmL = ((std::size_t) (grid_init_ptr + nel - nx - object_ptr->grid)) % nel;
		__m256i cvalmL = _mm256_loadu_si256(reinterpret_cast<__m256i*>(object_ptr->grid + ptrmL));


		// calculate current edge
		__m256i d1 = _mm256_sub_epi32(cval, cvalp1);
		d1 = _mm256_abs_epi32(d1);

		__m256i d2 = _mm256_sub_epi32(cval, cvalm1);
		d2 = _mm256_abs_epi32(d2);

		__m256i d3 = _mm256_sub_epi32(cval, cvalpL);
		d3 = _mm256_abs_epi32(d3);

		__m256i d4 = _mm256_sub_epi32(cval, cvalmL);
		d4 = _mm256_abs_epi32(d4);

		__m256i edge_current_x = _mm256_add_epi32(d1, d2);
		__m256i edge_current_y = _mm256_add_epi32(d3, d4);


		// calculate edges for raised surface at point i,j
		__m256i cvalraise = _mm256_add_epi32(cval, const_p1);
		d1 = _mm256_sub_epi32(cvalraise, cvalp1);
		d1 = _mm256_abs_epi32(d1);

		d2 = _mm256_sub_epi32(cvalraise, cvalm1);
		d2 = _mm256_abs_epi32(d2);

		d3 = _mm256_sub_epi32(cvalraise, cvalpL);
		d3 = _mm256_abs_epi32(d3);

		d4 = _mm256_sub_epi32(cvalraise, cvalmL);
		d4 = _mm256_abs_epi32(d4);

		__m256i edge_raise_x = _mm256_add_epi32(d1, d2);
		__m256i edge_raise_y = _mm256_add_epi32(d3, d4);

		__m256i nadd_edge_x = _mm256_sub_epi32(edge_raise_x, edge_current_x);
		__m256i nadd_edge_y = _mm256_sub_epi32(edge_raise_y, edge_current_y);


				// convert number of added edges to float
		__m256 fnadd_edge_x = _mm256_cvtepi32_ps(nadd_edge_x);
		__m256 fnadd_edge_y = _mm256_cvtepi32_ps(nadd_edge_y);

		fnadd_edge_x = _mm256_mul_ps(fnadd_edge_x, energy_edge_x);
		fnadd_edge_y = _mm256_mul_ps(fnadd_edge_y, energy_edge_y);


		__m256 total_edge_energy = _mm256_add_ps(fnadd_edge_x, fnadd_edge_y);
		_mm256_store_ps(raise_ptr, total_edge_energy);

		// add the difference in surface energy + bulk energy, no AVX mod operation, hope the compiler generates something nice here
		for(auto inner_idx = 0; inner_idx != 8; inner_idx ++)
		{
			raise_ptr[inner_idx] += object_ptr->surface_energies[(grid_init_ptr[inner_idx] + 1)%object_ptr->NLayer_types]
			                        - object_ptr->surface_energies[(grid_init_ptr[inner_idx])%object_ptr->NLayer_types]
			                        + object_ptr->bulk_energy[(grid_init_ptr[inner_idx] + 1)%object_ptr->NLayer_types];
		}

		// calculate edges for lowered surfaces

		__m256i cvallower = _mm256_sub_epi32(cval, const_p1);
		d1 = _mm256_sub_epi32(cvallower, cvalp1);
		d1 = _mm256_abs_epi32(d1);

		d2 = _mm256_sub_epi32(cvallower, cvalm1);
		d2 = _mm256_abs_epi32(d2);

		d3 = _mm256_sub_epi32(cvallower, cvalpL);
		d3 = _mm256_abs_epi32(d3);

		d4 = _mm256_sub_epi32(cvallower, cvalmL);
		d4 = _mm256_abs_epi32(d4);

		__m256i edge_lower_x = _mm256_add_epi32(d1, d2);
		__m256i edge_lower_y = _mm256_add_epi32(d3, d4);

		nadd_edge_x = _mm256_sub_epi32(edge_lower_x, edge_current_x);
		nadd_edge_y = _mm256_sub_epi32(edge_lower_y, edge_current_y);

		// convert number of added edges to float
		fnadd_edge_x = _mm256_cvtepi32_ps(nadd_edge_x);
		fnadd_edge_y = _mm256_cvtepi32_ps(nadd_edge_y);
		fnadd_edge_x = _mm256_mul_ps(fnadd_edge_x, energy_edge_x);
		fnadd_edge_y = _mm256_mul_ps(fnadd_edge_y, energy_edge_y);

		total_edge_energy = _mm256_add_ps(fnadd_edge_x, fnadd_edge_y);
		_mm256_store_ps(lower_ptr, total_edge_energy);

		// add the difference in surface energy, no AVX mod operation
		for(auto inner_idx = 0; inner_idx != 8; inner_idx ++)
		{
			lower_ptr[inner_idx] += object_ptr->surface_energies[(grid_init_ptr[inner_idx] - 1)%object_ptr->NLayer_types]
			                        - object_ptr->surface_energies[(grid_init_ptr[inner_idx])%object_ptr->NLayer_types]
			                        - object_ptr->bulk_energy[(grid_init_ptr[inner_idx])%object_ptr->NLayer_types];
		}

		// we need to exponentiate raise_ptr and lower_ptr
		__m256 raise = _mm256_load_ps(raise_ptr);

		Vec8f ag_raise(raise);
		ag_raise = -ag_raise;
		Vec8f exp_ag_raise = exp(ag_raise);
		raise = exp_ag_raise.intr_register();

		cumulative_sum_up = _mm256_add_ps(cumulative_sum_up, raise);
		_mm256_store_ps(raise_ptr, raise);
		_mm256_stream_ps(up_cumsum_ptr, cumulative_sum_up);

		__m256 lower = _mm256_load_ps(lower_ptr);

		Vec8f ag_lower(lower);
		ag_lower = -ag_lower;
		Vec8f exp_ag_lower = exp(ag_lower);
		lower = exp_ag_lower.intr_register();


		cumulative_sum_down = _mm256_add_ps(cumulative_sum_down, lower);
		_mm256_store_ps(lower_ptr, lower);
		_mm256_stream_ps(down_cumsum_ptr, cumulative_sum_down);

		if(object_ptr->needsEdgeGrid)
		{
			//__m256i total_edge = _mm256_add_epi32(edge_current_x, edge_current_y);
			_mm256_store_ps(edge_grid, cumulative_sum_down);
		}

		// increment pointers
		raise_ptr += INSTR_SET_SIZE;
		lower_ptr += INSTR_SET_SIZE;
		grid_init_ptr += INSTR_SET_SIZE;
		up_cumsum_ptr += INSTR_SET_SIZE;
		down_cumsum_ptr += INSTR_SET_SIZE;
		edge_grid += INSTR_SET_SIZE;
	}
	// store end of cumulative sums vectors
	_mm256_stream_ps(object_ptr->partial_csum_down + INSTR_SET_SIZE * thread_idx, cumulative_sum_down);
	_mm256_stream_ps(object_ptr->partial_csum_up + INSTR_SET_SIZE * thread_idx, cumulative_sum_up);

}

int KMC_Integrator::integrate(std::size_t N_events)
{
	if((grid_dim_x * grid_dim_y) % nthreads) // check that number of points in the grid is a integral multiple of the number of threads
	{
		return IntegrateErrorValues::number_points_not_multiple_number_threads;
	}

	if(grid_dim_x == 0 || grid_dim_y == 0 || !grid || !grid_prob_mov_up || !grid_prob_mov_down) // check that we are initialized
	{
		return IntegrateErrorValues::no_initialization;
	}
	std::size_t offset = grid_dim_x * grid_dim_y / nthreads;
	if(offset % INSTR_SET_SIZE) // number of points per thread needs to be a multiple integer of the vector register size
	{
		return IntegrateErrorValues::points_per_thread_not_instruction_compatible;
	}

	// RTYPE is float if macro SINGLE_PRECISION_RANDOM is set, double otherwise
	RTYPE* randoms = (typeof(randoms)) _mm_malloc(INSTR_SET_SIZE * sizeof(float), 32);


	Ranvec1 rd(3); // Random number generator using multiply with carry + mersene twister; from Agner Fog asm library
	if(N_events > 500)
	{
		// take the current time as seed
		rd.init((int) std::time(0));
	}
	else // using very few steps of MC is bad and you should feel bad (unless you're debugging)
	{
		// get garbage for a seed, bits from last 2 number should feel good plus add the bits from the time value cast as int64
		// not so random, but good enough for debugging purposes
		std::int64_t* ptr_to_garbage = reinterpret_cast<std::int64_t*>(randoms);
		std::int64_t* ptr_to_more_garbage = reinterpret_cast<std::int64_t*>(&time);
		rd.init((int) abs(*ptr_to_garbage + *ptr_to_more_garbage));
	}

	// vector of ones
	Vec8f ones8(1.0f);
	Vec4d ones4(1.0);

	for(auto event = 0; event != N_events; event++)
	{
		#ifdef SINGLE_PRECISION_RANDOM
		if(event % 2 == 0) // generate 8 random numbers, AVX2 instructions by Agner Fog
		{

			Vec8f rv = rd.random8f();
			rv = ones8 - rv;
			rv.store_a(randoms);
		}
		#else
		// use double precision for random variables
		Vec4d rv = rd.random4d();
		rv = ones4 - rv;
		rv.store_a(randoms);
		#endif


		// we have 8 floats before and after the grid to avoid bad loads from AVX instructions on the wrap-around
		memcpy(grid - INSTR_SET_SIZE, grid + grid_dim_x * grid_dim_y - INSTR_SET_SIZE, INSTR_SET_SIZE * sizeof(float));
		memcpy(grid + grid_dim_x * grid_dim_y, grid, INSTR_SET_SIZE * sizeof(float));

		std::thread threads[nthreads - 1];

		// this calls integrate_helper over nthreads
		// the helper is an assembly function that calculate transition probabilities exp(-E) of moving the surface up/down
		// and also calculate partial cumulative sums. Each threads generates 8 lines of partial cumulative sums, with
		// the final values stored in partial_csum_up and partial_csum_down, ordered by thread index and vector register
		for (auto i = 0; i != nthreads - 1; i++)
		{
			threads[i] = std::thread(integrate_helper, i+1, this);
		}
		integrate_helper(0, this);
		for (auto i = 0; i != nthreads - 1; i++)
		{
			threads[i].join();
		}

		// the cumulative sums are given into 8 partial sums per thread per direction (up/down) meaning we have only
		// 8 * nthreads * 2 overall moves to select first and then subselect into those
		float partial_up = std::accumulate(partial_csum_up, partial_csum_up + INSTR_SET_SIZE * nthreads, 0.0f);
		float partial_down = std::accumulate(partial_csum_down, partial_csum_down + INSTR_SET_SIZE * nthreads, 0.0f);

		// use the first four floats on even timesteps and the last four on odd timesteps
		RTYPE* rnd_ptr;
		rnd_ptr = randoms;
		#ifdef SINGLE_PRECISION_RANDOM
		if (event % 2 == 0)
			rnd_ptr = randoms;
		else
			rnd_ptr = randoms + 4;
		#endif

		// time step is given by -log(u(0,1)) / R, where R is the total rate ( sum of all exp(-E))
		time += -std::log(static_cast<double>(rnd_ptr[0])) / static_cast<double>(partial_up + partial_down);

		// find whether we're moving up or down
		bool mov_up = (static_cast<float>(rnd_ptr[1]) * (partial_down + partial_up)) > partial_down;
		float *pcumsum = mov_up ? partial_csum_up : partial_csum_down;

		// since the assembly generates a partial sum, treat the possible outcomes as a
		// (instruction_size * nthreads) * (Npoints / (instruction_size * nthreads)) matrix
		// first we find the line, then the column; this could potentially be optimized by using a supplementary
		// horizontal add in the assembly, first finding thread number, then vector register number then column


		// find which thread and thread line generated the move
		float gpos = static_cast<float>(rnd_ptr[2]) * std::accumulate(pcumsum, pcumsum + INSTR_SET_SIZE * nthreads, 0.0f);
		std::size_t line_index = 0;
		float pcnt = 0.0f;
		bool indexfound = false;
		for (auto idx = 0; idx != INSTR_SET_SIZE * nthreads; idx++)
		{
			pcnt += pcumsum[idx]; // partial sum accumulated on the fly
			if (gpos <= pcnt)
			{
				line_index = idx;
				indexfound = true;
				break;
			}
		}
		if(!indexfound)
			return IntegrateErrorValues::algorithm_error_line_search;

		// find in where in the line the move occured
		gpos = static_cast<float>(rnd_ptr[3]) * pcumsum[line_index];

		float *ogrid = mov_up ? grid_up_cumsum : grid_down_cumsum; // set the pointer to a valid grid

		//set initial index to correct thread and vector register number
		std::size_t initial_index = (line_index / INSTR_SET_SIZE) * offset + line_index % INSTR_SET_SIZE;
		std::size_t inc_idx = 0;
		bool incfound = false;

		// do a manual search. This can be parralelized by using nthreads to do partial sums, generate a random number
		// in that vector and do a subsearch
		for (auto mov_idx = 0; mov_idx != (offset / INSTR_SET_SIZE); mov_idx ++)
		{
			std::size_t abs_idx = initial_index + mov_idx * INSTR_SET_SIZE;
			if (gpos <= ogrid[abs_idx])
			{
				inc_idx = abs_idx;
				incfound = true;
				break;
			}
		}
		if(!incfound)
			return IntegrateErrorValues::algorithm_error_column_search;

		// move the corresponding index
		if (mov_up)
		{
			grid[inc_idx]++;
		}
		else
		{
			grid[inc_idx]--;
		}
	}
	_mm_free(randoms);
}

void KMC_Integrator::set_threads(std::size_t N)
{
	nthreads = N;
	_mm_free(partial_csum_down);
	_mm_free(partial_csum_up);
	partial_csum_down = (typeof(partial_csum_down)) _mm_malloc(INSTR_SET_SIZE * N * sizeof(*partial_csum_down), 64);
	partial_csum_up = (typeof(partial_csum_up)) _mm_malloc(INSTR_SET_SIZE * N * sizeof(*partial_csum_up), 64);
}


KMC_Integrator::~KMC_Integrator()
{
	free(surface_energies);
	free(bulk_energy);
	if(grid)
		_mm_free(grid - INSTR_SET_SIZE);
	_mm_free(grid_prob_mov_up);
	_mm_free(grid_prob_mov_down);
	_mm_free(grid_up_cumsum);
	_mm_free(grid_down_cumsum);
	_mm_free(edge_grid);
}

