
% case where we have two surfaces with different barriers
nmaxthreads = 1;
if(~isempty(getenv('PBS_NP')))
%nmaxthreads = min(16, str2num(getenv('PBS_NP')));
nmaxthreads = str2num(getenv('PBS_NP'));
end
disp(['Calculation using ', num2str(nmaxthreads), ' threads'])
gridsz = max(nmaxthreads, 16) * 8;
nsteps = 50000;
stepsz = 10000;
drive = -0.30;
% edge energy is 0.85 kT / edge
gres = [];
pbsarray = ~isempty(getenv('MOAB_JOBARRAYINDEX'));

if ~pbsarray
for(b = linspace(0, -drive, 10))
clear('gres');
integrator = CreateKMCIntegrator(nmaxthreads, [gridsz, gridsz], [0.85, 0.85], [0.25, 0.25 + b], [drive, drive]);
[gres, time] = StepKMCIntegrator(integrator, stepsz, nsteps);
gres = reshape(gres, [gridsz, gridsz, nsteps]);
save(['data_barrier_', num2str(b),'.mat'], '-v7.3');
DeleteKMCIntegrator(integrator);
end
else
ba = linspace(0, -drive, 1+str2num(getenv('MOAB_JOBARRAYRANGE')));
disp('Barrier Array : ')
disp(ba)
b = ba(1 + str2num(getenv('MOAB_JOBARRAYINDEX')));
disp(['Using barrier value of : ', num2str(b)]);
integrator = CreateKMCIntegrator(nmaxthreads, [gridsz, gridsz], [0.85, 0.85], [0.25, 0.25 + b], [drive, drive]);
[gres, time] = StepKMCIntegrator(integrator, stepsz, nsteps);
gres = reshape(gres, [gridsz, gridsz, nsteps]);
save(['FINALdata_barrier_', num2str(b),'.mat'], '-v7.3');
end

