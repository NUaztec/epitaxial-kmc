/*
 *
 * Copyright © 2017-2018 by Northwestern University. All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    The following paper should be cited:

 	Soyoung E. Seo, Martin Girard, Monica Olvera de la Cruz, Chad A. Mirkin, "Non-equilibrium anisotropic colloidal single
 	crystal growth with DNA", Nature Communications, in press

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef KW_MC_KMCINTEGRATOR_HPP
#define KW_MC_KMCINTEGRATOR_HPP

#include <cstdlib>
#include <cstdint>
#include <ctime>
#include <iostream>
#include "vectorclass.h"
#include "ranvec1.h"

//! Enum of handling errors within the KMC class
enum IntegrateErrorValues
{
	number_points_not_multiple_number_threads = -1,
	no_initialization = -2,
	points_per_thread_not_instruction_compatible = -3,
	algorithm_error_line_search = -4,
	algorithm_error_column_search = -5
};

//! A class to hold data in memory and perform integration
class KMC_Integrator
{
public :
	KMC_Integrator() {
		set_threads(1);
        display_license();
	};

	KMC_Integrator(std::size_t N) {
		set_threads(N);
        display_license();
	}

	void set_surface_dimensions(std::size_t L);

	void set_surface_dimensions(std::size_t Nx, std::size_t Ny);

	// different number of termination types
	void set_ntypes_surface(std::size_t N){ NLayer_types = N;}

	void set_threads(std::size_t N);

	void set_surface_edge_energies(float edge_energies_x,
								   float edge_energies_y);

	void set_surface_and_bulk_energies(float* senergies, float* benergies);

	void requireEdgeGrid(bool r);

	int integrate(std::size_t N_events);

	std::size_t get_numel();

	~KMC_Integrator();


	std::size_t NLayer_types = 1;
	std::size_t grid_dim_x = 0;
	std::size_t grid_dim_y = 0;

	float* surface_energies = nullptr;
	float edge_energy_x = 0.0;
	float edge_energy_y = 0.0;

	float* bulk_energy;

	std::int32_t* grid = nullptr;

	bool needsEdgeGrid = false;
	float* edge_grid = nullptr;

	// un normalized probability of adding one atom or removing one (exp(-deltaE))
	float* grid_prob_mov_up = nullptr;
	float* grid_prob_mov_down = nullptr;

	// grid of partial sums
	float* grid_up_cumsum = nullptr;
	float* grid_down_cumsum = nullptr;

	// subgrids to avoid reparsing
	float* partial_csum_up = nullptr;
	float* partial_csum_down = nullptr;

	std::size_t nthreads = 1;

	double time = 0.0;
    static bool do_license_display;

    void display_license() {
        if (!do_license_display)
            return;

        std::cout << "This program is free software: you can redistribute it and/or modify\n"
                     "    it under the terms of the GNU General Public License as published by\n"
                     "    the Free Software Foundation, either version 3 of the License, or\n"
                     "    (at your option) any later version.\n"
                     "\n"
                     "    This program is distributed in the hope that it will be useful,\n"
                     "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                     "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                     "    GNU General Public License for more details.\n"
                     "\n"
                     "    The following paper should be cited:\n"
                     "\n"
                     " \tSoyoung E. Seo, Martin Girard, Monica Olvera de la Cruz, Chad A. Mirkin, \"Non-equilibrium anisotropic colloidal single\n"
                     " \tcrystal growth with DNA\", Nature Communications, in press\n"
                     "\n"
                     "    You should have received a copy of the GNU General Public License\n"
                     "    along with this program.  If not, see <https://www.gnu.org/licenses/>." << std::endl;
        do_license_display = false;
    }

};

#endif //KW_MC_KMCINTEGRATOR_HPP
