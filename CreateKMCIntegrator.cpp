/*
 *
 *
 * This functions creates a KMCIntegrator object and returns a pointer to its location in memory
 *
 * Supplied arguments :
 * number of threads
 * grid dimensions; if a single value is supplied it will assume dimensions are equal
 * edge energies; if a single value is supplied, it will assume x and y edges have same energy
 * surface energies
 * bulk energies
 *
 */

#include <iostream>
#include "mex.h"
#include "KMCIntegrator.hpp"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if(nlhs == 0)
	{
		mexErrMsgTxt("No return value set for function, this would leak memory ! ");
	}
	if(nlhs > 1)
	{
		mexErrMsgTxt("Too many return values expected ! ");
	}


	KMC_Integrator* integrator_ptr = new KMC_Integrator();
	plhs[0] = mxCreateNumericMatrix(1,1, mxINT64_CLASS, mxREAL);
	std::uint64_t* retptr = (std::uint64_t*) mxGetData(plhs[0]);
	*retptr = (std::uint64_t) integrator_ptr;


	if(nrhs > 0)
		integrator_ptr->set_threads(static_cast<std::size_t>(*mxGetPr(prhs[0])));

	// grid dimensions specified
	if(nrhs > 1)
	{
		double* dim_ptr = mxGetPr(prhs[1]);
		std::size_t numeldims = mxGetNumberOfElements(prhs[1]);
		if(numeldims == 1)
			integrator_ptr->set_surface_dimensions(static_cast<float>(dim_ptr[0]));
		if(numeldims == 2)
			integrator_ptr->set_surface_dimensions(static_cast<float>(dim_ptr[0]),
												   static_cast<float>(dim_ptr[1]));
		if(numeldims>2)
			mexErrMsgTxt("Invalid number of dimensions specified, pointer returned is still valid");
	}


	if(nrhs > 2 && nrhs < 5)
		mexErrMsgTxt("Trying to set only one kind of energy for the calculations, surface and edges energies not set");

	if(nrhs > 2) // edge energies and surface energies set
	{
		double* edge_ptr = mxGetPr(prhs[2]);
		std::size_t numel_edge = mxGetNumberOfElements(prhs[2]);
		if(numel_edge == 1)
			integrator_ptr->set_surface_edge_energies(static_cast<float>(edge_ptr[0]), static_cast<float>(edge_ptr[0]));
		else
			integrator_ptr->set_surface_edge_energies(static_cast<float>(edge_ptr[0]), static_cast<float>(edge_ptr[1]));

		double* surf_ptr = mxGetPr(prhs[3]);
		std::size_t numel_surf = mxGetNumberOfElements(prhs[3]);
		double* bulk_ptr = mxGetPr(prhs[4]);
		std::size_t numel_bulk = mxGetNumberOfElements(prhs[4]);

		if(numel_surf != numel_bulk)
			mexErrMsgTxt("Number of bulk and surface energies don't match");

		integrator_ptr->set_ntypes_surface(numel_surf);
		// cast
		float* fsurf_ptr = (float*) malloc(sizeof(float) * numel_surf);
		float* fbulk_ptr = (float*) malloc(sizeof(float) * numel_bulk);
		for(auto idx = 0; idx != numel_surf; idx++)
		{
			fsurf_ptr[idx] = static_cast<float>(surf_ptr[idx]);
			fbulk_ptr[idx] = static_cast<float>(bulk_ptr[idx]);
		}

		integrator_ptr->set_surface_and_bulk_energies(fsurf_ptr, fbulk_ptr);
		free(fsurf_ptr);
		free(fbulk_ptr);
	}


	if(nrhs > 5)
		std::cout << "Warning : Unknown parameters discarded" << std::cout;


}