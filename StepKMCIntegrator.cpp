/*
 *
 *
 *
 * This function behaves differently depending on the number of arguments:
 *
 * grid = StepKMCIntegrator(pointer, steps); % will return the grid after steps MC events;
 *
 * [grid, time] = StepKMCIntegrator(pointer, steps, stepsize); % will step for an overall steps * stepsize MC events
 * returns :
 * grids: surface height every stepsize MC events
 * time: times between every stepsize MC events
 *
 * Additionaly, this function will make MATLAB save the data to disk under name data_barrier_B.mat, where B is the
 * energy difference between two layers. This operation is quite slow and an if statement should usually be inserted
 * before the call to "mexCallMATLAB()" to make it save less often.
 *
 */



#include "mex.h"
#include "KMCIntegrator.hpp"
#include <cstring>
#include <string>
#include <chrono>
#include <iostream>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if (nrhs < 1)
		mexErrMsgTxt("No pointer specified");
	if (nrhs < 2)
		mexErrMsgTxt("No number of event specified");

	std::uint64_t* ptr_to_ptr = (std::uint64_t*) mxGetData(prhs[0]);
	KMC_Integrator* ptr = reinterpret_cast<KMC_Integrator*>(*ptr_to_ptr);
	if(!ptr) // checking nullptr given
		mexErrMsgTxt("Null pointer given to step, aborting");

	if(nrhs == 2)
	{
		ptr->integrate(static_cast<std::size_t>(*mxGetPr(prhs[1])));

		if (nlhs == 1)
		{
			std::size_t nel = ptr->get_numel();
			plhs[0] = mxCreateDoubleMatrix(1, nel, mxREAL);
			double *rptr = mxGetPr(plhs[0]);
			for (auto idx = 0; idx != nel; idx++)
			{
				rptr[idx] = static_cast<double>(ptr->grid[idx]);
			}
		}
	}

	if(nrhs > 2)
	{
		// interpret first argument as step and second as number of time we repeat steps
		std::size_t n_integrator_steps = static_cast<std::size_t>(*mxGetPr(prhs[2]));
		std::size_t step_size = static_cast<std::size_t>(*mxGetPr(prhs[1]));
		std::size_t framesize = ptr->get_numel();

		if(nlhs == 0 && nrhs == 3)
		{
			ptr->integrate(n_integrator_steps * step_size);
		}
		else
		{
			std::int32_t* frame_ptr;
			if(nrhs > 3)
			{
				frame_ptr = (std::int32_t*) mxGetData(prhs[3]);
			} else if(nlhs > 0)
			{
				plhs[0] = mxCreateNumericMatrix(1, n_integrator_steps * framesize, mxINT32_CLASS, mxREAL);
				frame_ptr = (std::int32_t *) mxGetData(plhs[0]);
			}
			double* time_ptr = nullptr;
			if(nrhs == 5)
			{
				time_ptr = mxGetPr(prhs[4]);
			} else if(nlhs == 2)
			{
				plhs[1] = mxCreateDoubleMatrix(1, n_integrator_steps, mxREAL);
				time_ptr = mxGetPr(plhs[1]);
			}
			// create array for saving, done on stack to avoid memory issues
			mxArray* mxArrayStringPtr[2];
			std::string save_str1;
			save_str1 = "data_barrier_";
			float ediff = 0.0f;
			if (ptr->NLayer_types >= 2)
			{
				ediff = ptr->surface_energies[1] - ptr->surface_energies[0];
			}
			save_str1 += std::to_string(ediff) + std::string(".mat");
			std::string save_str2("-v7.3");
			mxArrayStringPtr[0] = mxCreateString(save_str1.c_str());
			mxArrayStringPtr[1] = mxCreateString(save_str2.c_str());

			for(auto fidx = 0; fidx != n_integrator_steps; fidx++)
			{
				//measure calculation time
				std::cout << "Time stepping index " << fidx << " out of " << n_integrator_steps << std::endl;
				auto start = std::chrono::high_resolution_clock::now();
				auto errcode = ptr->integrate(step_size);
				auto end = std::chrono::high_resolution_clock::now();

				std::chrono::duration<double> diff = end-start;

				if(errcode == -1)
				{
					mxDestroyArray(mxArrayStringPtr[0]);
					mxDestroyArray(mxArrayStringPtr[1]);
					mexErrMsgTxt("Number of threads is not an integer divisor of the number of grid points");
				}
				if(errcode == -2)
				{
					mxDestroyArray(mxArrayStringPtr[0]);
					mxDestroyArray(mxArrayStringPtr[1]);
					mexErrMsgTxt("Found uninitialized values");
				}
				if(errcode == -3)
				{
					mxDestroyArray(mxArrayStringPtr[0]);
					mxDestroyArray(mxArrayStringPtr[1]);
					mexErrMsgTxt("Vector offset not divisble by 0 (offset is given by number of grid points / nthreads)");
				}
				if(errcode == -4)
				{
					mxDestroyArray(mxArrayStringPtr[0]);
					mxDestroyArray(mxArrayStringPtr[1]);
					mexErrMsgTxt("Bad line index search");
				}
				if(errcode == -5)
				{
					mxDestroyArray(mxArrayStringPtr[0]);
					mxDestroyArray(mxArrayStringPtr[1]);
					mexErrMsgTxt("Bad column search");
				}
				// print current time steps per seconds
				std::cout << "KMC integrator : " << static_cast<double>(step_size) / diff.count()
				          << " steps per second  (" << step_size << " steps in " << diff.count()
				          << " seconds)" << std::endl;

				memcpy(frame_ptr, ptr->grid, framesize * sizeof(std::int32_t));
				frame_ptr += framesize;
				if(time_ptr != nullptr)
				{
					*time_ptr = ptr->time;
					time_ptr++;
				}

				// make matlab save current data
				mexCallMATLAB(0, nullptr, 2, mxArrayStringPtr, "save");
				auto true_end = std::chrono::high_resolution_clock::now();
				std::chrono::duration<double> diff2 = true_end - end;
				std::cout << "Rest of the operations (memcpy, save to disk) took : " << diff2.count() << " seconds " << std::endl;
			}
		}
	}
}