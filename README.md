# README #

A simple kinetic Monte-Carlo code, written in C / C++ with bindings for a MATLAB interface. To run this, copy the repository, run matlab _ build _ quest.m from within MATLAB to build the Monte-Carlo integrator mexa files. 

See matlab_simplerun.m for an example call to the functions

A few functions are provided :

CreateKMCIntegrator creates a KMC object with specified properties and returns a pointer to it. 
PointerToObject = CreateKMCIntegrator(NumberOfThreads, [GridSizeX, GridSizeY], [EdgeEnergyX, EdgeEnergyY], [SurfaceEnergyA, SurfaceEnergyB, ..., SurfaceEnergyX], [BulkEnergyA, BulkEnergyB, ..., BulkEnergyX]);

# StepKMCIntegrator has a few different uses : #

NewGrid = StepKMCIntegrator(PointerToObject, NumberOfSteps)
This will generate NumberOfSteps MC events and return the surface in a *linear* array of size GridSizeX by GridSizeY

[Frames, Time] = StepKMCIntegrator(PointerToObject, NumberOfSteps, NumberOfRepeats)
This will generate NumberOfSteps MC events, store the frame into Frames, the time into Time and will repeat the process NumberOfRepeats times. Frames is a GridSizeX by GridSizeY by NumberOfRepeats linear vector and Time is a linear vector of size NumberOfRepeats

StepKMCIntegrator(PointerToObject, NumberOfSteps, NumberOfRepeats, Frames, Time)
This will generate NumberOfSteps MC events, store the frame into Frames and time into Time with NumberOfRepeats times. Frame and Time must be long enough to contain the data requested. The mexa function will call the matlab save function in order to save the data to disk.

# License

Only the C++ part (KMCIntegrator.cpp and KMCIntegrator.hpp) is published under GPL license, any files that includes mex.h or contains matlab source code is free of any copyright.  