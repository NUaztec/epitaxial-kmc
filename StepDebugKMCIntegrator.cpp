/*
 *
 *
 *
 * This function behaves differently depending on the number of arguments:
 *
 * grid = StepDebugKMCIntegrator(pointer, steps); % will perform steps MC events and return the surface grid
 *
 *  StepDebugKMCIntegrator(pointer, steps, stepsize); % will perform steps * stepsize MC events
 *
 *  [grids, time, edges] = StepDebugKMCIntegrator(pointer, steps, stepsize); % will perform steps * stepsize MC events
 *
 *  grids contains the full surface grid printed every stepsize events;
 *  time contains all the times for every stepsize events;
 *  edges contains the number of edges of everypoint of the grid, printed every stepsize events
 *
 */


#include "mex.h"
#include "KMCIntegrator.hpp"
#include <cstring>


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if (nrhs < 1)
		mexErrMsgTxt("No pointer specified");
	if (nrhs < 2)
		mexErrMsgTxt("No number of event specified");

	std::uint64_t* ptr_to_ptr = (std::uint64_t*) mxGetData(prhs[0]);
	KMC_Integrator* ptr = reinterpret_cast<KMC_Integrator*>(*ptr_to_ptr);
	if(!ptr) // checking nullptr given
		mexErrMsgTxt("Null pointer given to step, aborting");

	if(nrhs == 2)
	{
		ptr->integrate(static_cast<std::size_t>(*mxGetPr(prhs[1])));

		if (nlhs == 1)
		{
			std::size_t nel = ptr->get_numel();
			plhs[0] = mxCreateDoubleMatrix(1, nel, mxREAL);
			double *rptr = mxGetPr(plhs[0]);
			for (auto idx = 0; idx != nel; idx++)
			{
				rptr[idx] = static_cast<double>(ptr->grid[idx]);
			}
		}
	}

	if(nrhs == 3)
	{
		// interpret first argument as step and second as number of time we repeat steps
		std::size_t n_integrator_steps = static_cast<std::size_t>(*mxGetPr(prhs[2]));
		std::size_t step_size = static_cast<std::size_t>(*mxGetPr(prhs[1]));
		std::size_t framesize = ptr->get_numel();

		if(nlhs == 0)
		{
			ptr->integrate(n_integrator_steps * step_size);
		}
		else
		{
			plhs[0] = mxCreateNumericMatrix(1, n_integrator_steps * framesize, mxINT32_CLASS, mxREAL);
			std::int32_t* frame_ptr = (std::int32_t*) mxGetData(plhs[0]);

			double* time_ptr = nullptr;
			if(nlhs > 1)
			{
				plhs[1] = mxCreateDoubleMatrix(1, n_integrator_steps, mxREAL);
				time_ptr = mxGetPr(plhs[1]);
			}

			float* edge_ptr = nullptr;
			if(nlhs > 2) // return grid of # edges
			{
				plhs[2] = mxCreateNumericMatrix(1, n_integrator_steps * framesize, mxSINGLE_CLASS, mxREAL);
				edge_ptr = (float*) mxGetData(plhs[2]);
				ptr->requireEdgeGrid(true);
			}

			for(auto fidx = 0; fidx != n_integrator_steps; fidx++)
			{
				auto errcode = ptr->integrate(step_size);
				if(errcode == -1)
				{
					mexErrMsgTxt("Number of threads is is an integer divisor of the number of grid points");
				}
				if(errcode == -2)
				{
					mexErrMsgTxt("Found uninitialized values");
				}
				if(errcode == -3)
				{
					mexErrMsgTxt("Vector offset not divisble by 0 (offset is given by number of grid points / nthreads)");
				}
				if(errcode == -4)
				{
					mexErrMsgTxt("Bad line index search");
				}
				if(errcode == -5)
				{
					mexErrMsgTxt("Bad column search");
				}
				memcpy(frame_ptr, ptr->grid, framesize * sizeof(std::int32_t));
				frame_ptr += framesize;
				if(time_ptr != nullptr)
				{
					*time_ptr = ptr->time;
					time_ptr++;
				}
				if(edge_ptr != nullptr)
				{
					memcpy(edge_ptr, ptr->edge_grid, framesize * sizeof(*edge_ptr));
					edge_ptr+=framesize;
				}
			}
		}
	}
}