/*
 *
 *
 * This functions deletes a KMCIntegrator object located at the supplied pointer
 */

#include "mex.h"
#include "KMCIntegrator.hpp"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if (nrhs < 1)
		mexErrMsgTxt("No pointer specified");

	std::uint64_t* ptr_to_ptr = (std::uint64_t*) mxGetData(prhs[0]);

	KMC_Integrator* ptr = reinterpret_cast<KMC_Integrator*>(*ptr_to_ptr);
	delete ptr;
	// set the previous pointer to null in case this is called twice to avoid problems
	*ptr_to_ptr = 0;
}
